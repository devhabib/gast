(function ($, window, document, undefined) {
  var $win = $(window);

  function savingState(show, limit_to) {
    show = typeof show !== 'undefined' ? show : true;
    limit_to = typeof limit_to !== 'undefined' ? limit_to : false;
    if (limit_to) {
      var $savingStateDIV = limit_to.find('li.active .savingState, .topSavingState.savingState, .calendarSavingState');
      var $stuffToHide = limit_to.find('.monthName');
      var $stuffToTransparent = limit_to.find('table.booked-calendar tbody');
    } else {
      var $savingStateDIV = $('li.active .savingState, .topSavingState.savingState, .calendarSavingState');
      var $stuffToHide = $('.monthName');
      var $stuffToTransparent = $('table.booked-calendar tbody');
    }
    if (show) {
      $savingStateDIV.fadeIn(200);
      $stuffToHide.hide();
      $stuffToTransparent.animate({
        'opacity': 0.2
      }, 100);
    } else {
      $savingStateDIV.hide();
      $stuffToHide.show();
      $stuffToTransparent.animate({
        'opacity': 1
      }, 0);
    }
  }

  function init_appt_list_date_picker() {
    $('.booked_list_date_picker').each(function () {
      var thisDatePicker = $(this);
      var minDateVal = thisDatePicker.parents('.booked-appt-list').attr('data-min-date');
      var maxDateVal = thisDatePicker.parents('.booked-appt-list').attr('data-max-date');
      if (typeof minDateVal == 'undefined') {
        var minDateVal = thisDatePicker.attr('data-min-date');
      }
      thisDatePicker.datepicker({
        dateFormat: 'yy-mm-dd',
        minDate: minDateVal,
        maxDate: maxDateVal,
        showAnim: false,
        beforeShow: function (input, inst) {
          $('#ui-datepicker-div').removeClass();
          $('#ui-datepicker-div').addClass('booked_custom_date_picker');
        },
        onClose: function (dateText) {
          $('.booked_list_date_picker_trigger').removeClass('booked-dp-active');
        },
        onSelect: function (dateText) {
          var thisInput = $(this),
            date = dateText,
            thisList = thisInput.parents('.booked-list-view'),
            defaultDate = thisList.attr('data-default'),
            calendar_id = thisInput.parents('.booked-list-view-nav').attr('data-calendar-id');
          if (typeof defaultDate == 'undefined') {
            defaultDate = false;
          }
          if (!calendar_id) {
            calendar_id = 0;
          }
          thisList.addClass('booked-loading');
          var booked_load_list_view_date_booking_options = {
            'action': 'booked_appointment_list_date',
            'date': date,
            'calendar_id': calendar_id,
            'force_default': defaultDate
          };
          $(document).trigger("booked-before-loading-appointment-list-booking-options");
          thisList.spin('booked_top');
          $.ajax({
            url: booked_js_vars.ajax_url,
            type: 'post',
            data: booked_load_list_view_date_booking_options,
            success: function (html) {
              thisList.html(html);
              init_appt_list_date_picker();
              setTimeout(function () {
                thisList.removeClass('booked-loading');
              }, 1);
            }
          });
          return false;
        }
      });
    });
    $('body').on('click', '.booked_list_date_picker_trigger', function (e) {
      e.preventDefault();
      if (!$(this).hasClass('booked-dp-active')) {
        $(this).addClass('booked-dp-active');
        $(this).parents('.booked-appt-list').find('.booked_list_date_picker').datepicker('show');
      }
    });
  }
  var BookedTabs = {
    bookingModalSelector: '.booked-modal',
    tabSelector: '.booked-tabs',
    tabNavSelector: '.booked-tabs-nav span',
    tabCntSelector: '.booked-tabs-cnt',
    Init: function () {
      $(document).on('click', this.tabNavSelector, this.tabsNav);
    },
    tabsNav: function (event) {
      event.preventDefault();
      BookedTabs.switchToTab($(this));
      BookedTabs.maybeResizeBookingModal();
    },
    switchToTab: function (tab_nav_item) {
      var $nav_item = tab_nav_item,
        tab_cnt_class = '.' + $nav_item.data('tab-cnt'),
        $tabs_container = $nav_item.parents(BookedTabs.tabSelector);
      $nav_item.addClass('active').siblings().removeClass('active')
      $tabs_container.find(BookedTabs.tabCntSelector + ' ' + tab_cnt_class).addClass('active').siblings().removeClass('active');
    },
    maybeResizeBookingModal: function () {
      if (!$(BookedTabs.bookingModalSelector).length) {
        return;
      }
      resize_booked_modal();
    }
  }
})(jQuery, window, document);

function create_booked_modal() {
  var windowHeight = jQuery(window).height();
  var windowWidth = jQuery(window).width();
  if (windowWidth > 720) {
    var maxModalHeight = windowHeight - 295;
  } else {
    var maxModalHeight = windowHeight;
  }
  jQuery('body input, body textarea, body select').blur();
  jQuery('body').addClass('booked-noScroll');
  jQuery('<div class="booked-modal bm-loading"><div class="bm-overlay"></div><div class="bm-window"><div style="height:100px"></div></div></div>').appendTo('body');
  jQuery('.booked-modal .bm-overlay').spin('booked_white');
  jQuery('.booked-modal .bm-window').css({
    'max-height': maxModalHeight + 'px'
  });
}
var previousRealModalHeight = 100;

function resize_booked_modal() {
  var windowHeight = jQuery(window).height();
  var windowWidth = jQuery(window).width();
  var common43 = 43;
  if (jQuery('.booked-modal .bm-window .booked-scrollable').length) {
    var realModalHeight = jQuery('.booked-modal .bm-window .booked-scrollable')[0].scrollHeight;
    if (realModalHeight < 100) {
      realModalHeight = previousRealModalHeight;
    } else {
      previousRealModalHeight = realModalHeight;
    }
  } else {
    var realModalHeight = 0;
  }
  var minimumWindowHeight = realModalHeight + common43 + common43;
  var modalScrollableHeight = realModalHeight - common43;
  var maxModalHeight;
  var maxFormHeight;
  if (windowHeight < minimumWindowHeight) {
    modalScrollableHeight = windowHeight - common43 - common43;
  } else {
    modalScrollableHeight = realModalHeight;
  }
  if (windowWidth > 720) {
    maxModalHeight = modalScrollableHeight - 25;
    maxFormHeight = maxModalHeight - 15;
    var modalNegMargin = (maxModalHeight + 78) / 2;
  } else {
    maxModalHeight = windowHeight - common43;
    maxFormHeight = maxModalHeight - 60;
    var modalNegMargin = (maxModalHeight) / 2;
  }
  jQuery('.booked-modal').css({
    'margin-top': '-' + modalNegMargin + 'px'
  });
  jQuery('.booked-modal .bm-window').css({
    'max-height': maxModalHeight + 'px'
  });
  jQuery('.booked-modal .bm-window .booked-scrollable').css({
    'max-height': maxFormHeight + 'px'
  });
}

function close_booked_modal() {
  var modal = jQuery('.booked-modal');
  modal.fadeOut(200);
  modal.addClass('bm-closing');
  jQuery('body').removeClass('booked-noScroll');
  setTimeout(function () {
    modal.remove();
  }, 300);
}


