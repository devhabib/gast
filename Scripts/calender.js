if (window.jQuery) {
    self();
} else {
    console.log('Load jQuery First');
}

let entry_block = `
<tr class="entryBlock">
<td colspan="7">
    <div class="booked-appt-list shown" style="display: block;">
    <h2><span>Available Appointment on </span><strong>May 26,
        2020</strong><span></span></h2>
    <div class="timeslot bookedClearFix"><span class="timeslot-time"><span class="timeslot-range"><i
            class="booked-icon booked-icon-clock"></i>&nbsp;&nbsp;All
            day</span><span class="spots-available">1 space
            available</span></span><span class="timeslot-people"><button data-calendar-id="545"
            data-title="" data-timeslot="0000-2400" data-date="2020-05-26" class="new-appt button"><span
            class="button-timeslot">All day</span><span class="button-text">Book Appointment</span><span
            class="spots-available">1 space available</span></button></span>
    </div>
    </div>
</td>
</tr>`

function self() {
    $('.date').tooltipster({
        animation: 'fade',
        delay: 200,
        theme: 'tooltipster-shadow',
    });

    $('.number').on('click', (e) => {
        let entry = $('.entryBlock');
        let booked = $(e.target).closest('.booked');
        let activeClass = $('td.active');

        if (!booked.length) {
            entry.remove();
            activeClass.removeClass('active');
            $(e.target).closest('td').addClass('active');
            $(e.target).closest('tr').after(entry_block);
        } else {
            activeClass.removeClass('active');
            entry.remove();
        }
        e.stopPropagation();
    });

}